//JYOTHIS RAJAN THELLIKUZHIEL
//C0744903

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class BlazeDemoTest {
	//Global driver variables
	WebDriver driver;
	
	//Location of chromedriver file
	final String ChromeDriveLocation = "/Users/jyothisrajan/Desktop/chromedriver";
	
	//Website we want to test
	final String baseUrl = "http://www.BlazeDemo.com";

	@Before
	public void setUp() throws Exception {
		// Selenium setup

		System.setProperty("webdriver.chrome.driver", ChromeDriveLocation);

		driver = new ChromeDriver();

		// 2. go to website

		driver.get(baseUrl);
	}

	@After
	public void tearDown() throws Exception {
		
		// After you run the test case, close the browser

		Thread.sleep(5000);

		driver.close();
	}

	@Test
	public void test() {
		//calling first testcase function
		TestTC1();
	}
	
	//function for TC1
	void TestTC1() {
		Select dropDown = new Select(driver.findElement(By.name("fromPort")));
		

		List<WebElement> list = dropDown.getOptions();
		assertEquals(7, list.size());
		
	}
	
	void TestTC3() {
		//5.Presses button
		WebElement findButton = driver.findElement(By.id("btn btn-primary"));

		findButton.click();
		
	}

}
