import static org.junit.Assert.*;
//JYOTHIS RAJAN THELLIKUZHIEL
//C0744903

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SnakeTest {
	Snake snakeobj1,snakeobj2;
	@Before
	public void setUp() throws Exception {
		//creating Snake objects
		
		snakeobj1=new Snake("Peter",10,"coffee");
		snakeobj2=new Snake("Takisr",80,"vegetables");
	}

	@After
	public void tearDown() throws Exception {
		
	}

	@Test
	public void test() {
		//First test case
		
		String testResult1 = String.valueOf(snakeobj1.isHealthy());
		assertEquals(testResult1,"false");

		//second test case
		
		String testResult2 = String.valueOf(snakeobj2.fitsInCage(100));
		assertEquals(testResult2,"true");
		
	}

}
